let
  sources = import ./npins;
  pkgs = import sources.nixpkgs { };
in
with pkgs;
mkShell {
  nativeBuildInputs = [ meson ninja pkg-config wayland-scanner wayland-protocols ];
  buildInputs = [ libev opencl-headers ocl-icd boost wayland ];
}
